import {
  shapeSimilarity
} from 'curve-matcher'

let obj =  {
  run: (stroke, data, threshold) => {
    let candiates = []
    if (!stroke || stroke.length <= 1) return
    let keys = Object.keys(data)
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i]
      let candidateStrokes = data[key].strokes
      for (let j = 0; j < candidateStrokes.length; j++) {
        let candidateStroke = candidateStrokes[j]
        let score = obj.match(stroke, candidateStroke)
        if (score >= threshold) {
          let pattern = key
          candiates.push({ pattern, score })
        }
      }
    }
    return candiates.sort((a, b) => parseFloat(b.score) - parseFloat(a.score))
  },

  match: (path, candidatePath) => {
    return shapeSimilarity(path, candidatePath, {
      restrictRotationAngle: 0.5235988// 30 degree in radians
    });
  }
}

export default obj;
